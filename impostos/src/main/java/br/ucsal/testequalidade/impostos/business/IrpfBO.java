package br.ucsal.testequalidade.impostos.business;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.ucsal.testequalidade.impostos.domain.FichaFinanceira;
import br.ucsal.testequalidade.impostos.domain.Funcionario;

public class IrpfBO {

	private static final BigDecimal ALIQUOTA_1 = new BigDecimal("0.075");
	private static final BigDecimal ALIQUOTA_2 = new BigDecimal("0.150");
	private static final BigDecimal ALIQUOTA_3 = new BigDecimal("0.225");
	private static final BigDecimal ALIQUOTA_4 = new BigDecimal("0.275");
	private static final BigDecimal FAIXA_1_TOPO = new BigDecimal("1903.98");
	private static final BigDecimal FAIXA_2_TOPO = new BigDecimal("2826.65");
	private static final BigDecimal FAIXA_3_TOPO = new BigDecimal("3751.05");
	private static final BigDecimal FAIXA_4_TOPO = new BigDecimal("4664.68");

	private IrpfBO() {
	}

	public static BigDecimal calcularIr(FichaFinanceira fichaFinanceira) {
		if (fichaFinanceira.getFuncionario().isPossuiDoencaGrave()) {
			return new BigDecimal("0.00");
		}
		BigDecimal baseCalculo = fichaFinanceira.getSalarioBruto();
		BigDecimal baseCalculoFaixa;
		BigDecimal ir = BigDecimal.ZERO;
		if (baseCalculo.compareTo(FAIXA_4_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_4_TOPO);
			ir = ir.add(baseCalculoFaixa.multiply(ALIQUOTA_4).setScale(2, RoundingMode.HALF_EVEN));
			baseCalculo = FAIXA_4_TOPO;
		}
		if (baseCalculo.compareTo(FAIXA_3_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_3_TOPO);
			ir = ir.add(baseCalculoFaixa.multiply(ALIQUOTA_3).setScale(2, RoundingMode.HALF_EVEN));
			baseCalculo = FAIXA_3_TOPO;
		}
		if (baseCalculo.compareTo(FAIXA_2_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_2_TOPO);
			ir = ir.add(baseCalculoFaixa.multiply(ALIQUOTA_2).setScale(2, RoundingMode.HALF_EVEN));
			baseCalculo = FAIXA_2_TOPO;
		}
		if (baseCalculo.compareTo(FAIXA_1_TOPO) > 0) {
			baseCalculoFaixa = baseCalculo.subtract(FAIXA_1_TOPO);
			ir = ir.add(baseCalculoFaixa.multiply(ALIQUOTA_1).setScale(2, RoundingMode.HALF_EVEN));
		}
		return ir.setScale(2, RoundingMode.HALF_EVEN);
	}

}
